﻿using System;
using System.IO;
using System.Threading;

namespace M09UF2E1_CalvoArroyoEric
{
    class Ex1
    {
        public static void Main()
        {
            Directory.SetCurrentDirectory(@"..\..\..\Docs");
            Console.WriteLine(Directory.GetCurrentDirectory());
            Threading();
        }
        public static void Threading()
        {
            Thread x = new Thread(DocReader);
            Thread y = new Thread(DocReader);
            Thread z = new Thread(DocReader);
            x.Start("klk.txt");
            y.Start("carambaquetarde.txt");
            z.Start("mequieromorir.txt");
        }
        public static void DocReader(Object path)
        {
            string stringPath = (string)path;
            using (StreamReader doc = new StreamReader(stringPath))
            {
                int contador = 0;
                while (doc.ReadLine() != null)
                {
                    contador++;
                }
                doc.Close();
                Console.WriteLine("El doc té: " + contador + " línies.");
            }
        }
    }

}
